from fastapi import HTTPException, status


CredentialsException = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)

InactiveUserException = HTTPException(
    status_code=400,
    detail="Inactive user",
)

PasswordConfirmException = HTTPException(
    status_code=400,
    detail="Password confirmation is different to original",
)

UserAlreadyExistsException = HTTPException(
    status_code=401,
    detail="Inactive user",
)


ExternalAPIException = HTTPException(
    status_code=400,
    detail="Api is not available"
)
