from fastapi.testclient import TestClient

from app.database.connection import get_db
from app.main import app
from app.tests.direction.fixtures import direction_create

app.dependency_overrides[get_db] = get_db
client = TestClient(app)


def check_time_create():
    for i in range(10000):
        resp = client.post("/directions/", json=direction_create)
        assert resp.status_code == 200
    print("directions create checked")


check_time_create()
