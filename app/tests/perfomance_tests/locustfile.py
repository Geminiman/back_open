from locust import HttpUser, task, between
from app.tests.direction.fixtures import direction_create
import random


class QuickstartUser(HttpUser):
    wait_time = between(1, 2.5)

    @task(2)
    def directions_get(self):
        direction_id = random.randrange(1, 10000)
        self.client.get(f"directions/{direction_id}")

    @task(3)
    def directions_create(self):
        self.client.post("directions/", json=direction_create)
