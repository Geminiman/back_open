from fastapi.testclient import TestClient

from app.database.connection import get_db
from app.main import app
from app.tests.user.fixtures import user_create

app.dependency_overrides[get_db] = get_db
client = TestClient(app)


def test_sign_in():
    client.post("/users/sign_up", json=user_create)
    response = client.post("/users/sign_in", json=user_create)
    assert response.status_code == 200, response.text
    assert response.json()["access_token"] != ''


def test_current_user():
    pass


def test_sign_out():
    pass
