from fastapi.testclient import TestClient

from app.database.connection import get_db
from app.main import app
from app.tests.direction.fixtures import direction_create, direction_read, direction_update, direction_update_response

app.dependency_overrides[get_db] = get_db
client = TestClient(app)


def test_index():
    client.post("/directions/", json=direction_create)
    client.post("/directions/", json=direction_update)
    response = client.get("/directions/")
    assert response.status_code == 200
    assert response.json() != []


def test_create():
    response = client.post("/directions/", json=direction_create)
    assert response.status_code == 200, response.text
    data = response.json()
    assert "id" in data
    direction_id = data["id"]

    response = client.get(f"/directions/{direction_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    del data["id"]
    assert data == direction_read


def test_update():
    response = client.post("/directions/", json=direction_create)
    direction_id = response.json()["id"]
    response = client.put(f"/directions/{direction_id}", json=direction_update)
    assert response.status_code == 200, response.text

    response = client.get(f"/directions/{direction_id}")
    assert response.status_code == 200, response.text
    data = response.json()
    del data["id"]
    assert data == direction_update_response


def test_delete():
    response = client.post("/directions/", json=direction_create)
    direction_id = response.json()["id"]
    response = client.put(f"/directions/{direction_id}", json=direction_update)
    assert response.status_code == 200, response.text

    response = client.delete(f"/directions/{direction_id}")
    assert response.status_code == 204
