direction_create = {
    "title": "direction_name",
    "description": "description_name",
    "length": 455,
    "checkpoints": [[42.1232, 83.1233]],
    "checkpoints_titles": ["string"],
    "owner_id": None,
    "polyline": "gmfulAno~blClfAshDjuAz}@"
}

direction_update = {
    "title": "direction_name_update",
    "description": "description_name_update",
    "length": 456,
    "owner_id": None,
    "segments": "gmfulAno~blClfAshDjuAz}@"
}

direction_update_response = {
    "title": "direction_name_update",
    "description": "description_name_update",
    "length": 456,
    "owner_id": None,
    "segments": [
        [52.530984, 13.384567], [52.53096, 13.38448], [52.53082, 13.38398], [52.5307, 13.38351]
    ]
}

direction_read = {
    "name": "direction_name",
    "description": "description_name",
    "length": 455,
    "owner_id": None,
    "segments": [
        [52.530984, 13.384567], [52.53096, 13.38448], [52.53082, 13.38398], [52.5307, 13.38351]
    ]
}
