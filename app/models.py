import enum

from sqlalchemy import Column, Integer, String, Float, ForeignKey, ARRAY, Boolean, Enum, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import CIDR

from app.database.database import Base
from app.sqlalchemy_mixins import DateTimeMixin


class TransportTypeEnum(enum.IntEnum):
    any = 0
    walk = 1
    bycicle = 2
    car = 3


class User(Base, DateTimeMixin):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, index=True, unique=True)
    username = Column(String)
    password_hash = Column(String)
    disabled = Column(Boolean, nullable=True, default=False)

    directions = relationship("Direction", back_populates="owner")


class Direction(Base, DateTimeMixin):
    __tablename__ = "directions"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    description = Column(String)
    length = Column(Float)
    route_points = Column(ARRAY(Float))
    stop_points = Column(ARRAY(Float))
    stop_points_titles = Column(ARRAY(String))
    is_public = Column(Boolean, nullable=False, default=False)
    transport_type = Column(Enum(TransportTypeEnum), nullable=True)
    owner_id = Column(Integer, ForeignKey("users.id"), nullable=True)

    owner = relationship("User", back_populates="directions")


class UserSession(Base, DateTimeMixin):
    __tablename__ = "user_sessions"
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, nullable=False)
    ip = Column(CIDR, nullable=False)
    os = Column(String, nullable=True)
    browser = Column(String, nullable=True)
    user_agent = Column(String, nullable=True)
    token = Column(String, nullable=False)
    is_revoked = Column(Boolean, default=False, nullable=False)
    expired_at = Column(DateTime(timezone=True), nullable=False)
