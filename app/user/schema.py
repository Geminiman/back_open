from typing import Optional

from pydantic import BaseModel


class UserBase(BaseModel):
    email: str
    username: Optional[str]


class UserCreate(UserBase):
    password: str
    password_confirmation: str


class UserSignIn(UserBase):
    password: str


class User(UserBase):
    id: int
    disabled: Optional[bool] = None

    class Config:
        orm_mode = True
