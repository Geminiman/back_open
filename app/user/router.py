from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session as DBSession

from app.database.connection import get_db
from app.direction import schema as direction_schema
from app.direction import crud as direction_crud
from app.user import schema, auth
from app.user.auth import Token, get_current_active_user, oauth2_scheme

router = APIRouter(
    prefix="/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)


@router.post("/sign_up", response_model=Token)
async def sign_up(user: schema.UserCreate, dbsession: DBSession = Depends(get_db)):
    return await auth.sign_up(dbsession=dbsession, user=user)


@router.post("/sign_in", response_model=Token)
async def sign_in(user: schema.UserSignIn, dbsession: DBSession = Depends(get_db)):
    return await auth.sign_in(dbsession=dbsession, user=user)


@router.get("/current_user", response_model=schema.User)
async def user_data(current_user: schema.User = Depends(get_current_active_user)):
    return current_user


@router.get("/current_user/directions", response_model=List[direction_schema.Direction])
async def user_directions(
        current_user: schema.User = Depends(get_current_active_user),
        dbsession: DBSession = Depends(get_db)
):
    directions = await direction_crud.get_user_directions(dbsession, current_user.id)
    return directions


@router.post('/refresh')
async def refresh(token: str = Depends(oauth2_scheme), dbsession: DBSession = Depends(get_db)):
    return await auth.refresh_all_tokens(dbsession=dbsession, refresh_token=token)


@router.post("/sign_out")
async def sign_out():
    return "signed out"
