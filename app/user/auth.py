from datetime import datetime, timedelta
from ipaddress import ip_network
from typing import Dict

from fastapi import Depends
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from passlib.context import CryptContext
from pydantic import BaseModel
from sqlalchemy.orm import Session as DBSession

from app.database.connection import get_db
from app.exceptions import CredentialsException, InactiveUserException, PasswordConfirmException, \
    UserAlreadyExistsException
from app.settings import SECRET_KEY, HASHING_ALGORITHM, ACCESS_TOKEN_EXPIRE_MINUTES, REFRESH_TOKEN_SECRET, \
    REFRESH_TOKEN_EXPIRE_MINUTES
from app.user import crud, schema
from app.user.crud import get_user_by_email, get_user_by_id, get_user_session, update_refresh_token, \
    create_user_session
from app.user.schema import User

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


class Token(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str


class TokenData(BaseModel):
    user_id: str


def verify_password(plain_password: str, hashed_password: str) -> bool:
    is_verified = pwd_context.verify(plain_password, hashed_password)
    if not is_verified:
        raise CredentialsException
    return True


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


async def authenticate_user(session: DBSession, email: str, password: str):
    user = await get_user_by_email(dbsession=session, email=email)
    if not user or not verify_password(password, user.password_hash):
        raise CredentialsException
    return user


def create_access_token(data: dict, expires_delta: timedelta = timedelta(minutes=15)) -> str:
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=HASHING_ALGORITHM)
    return encoded_jwt


def create_refresh_token(data: dict, expires_delta: timedelta = timedelta(minutes=60*24*2)) -> str:
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, REFRESH_TOKEN_SECRET, algorithm=HASHING_ALGORITHM)
    return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_scheme), dbsession: DBSession = Depends(get_db)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[HASHING_ALGORITHM])
        sub: str = payload.get("sub", None)
        user_id: int = int(sub)
        if user_id is None:
            raise CredentialsException
        token_data = TokenData(user_id=user_id)
    except JWTError:
        raise CredentialsException

    user = await get_user_by_id(dbsession=dbsession, user_id=int(token_data.user_id))
    if user.disabled:
        raise InactiveUserException
    if user is None:
        raise CredentialsException
    return user


async def get_current_active_user(current_user: User = Depends(get_current_user)):
    if current_user.disabled:
        raise InactiveUserException
    return current_user


async def sign_up(dbsession: DBSession, user: schema.UserCreate) -> Dict[str, str]:
    if user.password != user.password_confirmation:
        raise PasswordConfirmException
    try:
        user = await crud.create_user(dbsession=dbsession, user=user)
    except:
        raise UserAlreadyExistsException
    access_token = create_access_token(
        data={"sub": str(user.id)},
        expires_delta=timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    )
    refresh_expire_time = timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES)
    refresh_token = create_refresh_token(
        data={"sub": str(user.id)},
        expires_delta=refresh_expire_time
    )
    user_session_data = {
        "user_id": user.id,
        "ip": ip_network("192.168.1.0"),
        "token": refresh_token,
        "expired_at": datetime.now() + refresh_expire_time
    }
    await create_user_session(dbsession=dbsession, user_session_data=user_session_data)
    return {
        "access_token": access_token,
        "refresh_token": refresh_token,
        "token_type": "bearer"
    }


async def sign_in(dbsession: DBSession, user: schema.UserSignIn) -> Dict[str, str]:
    signed_user = await authenticate_user(session=dbsession, email=user.email, password=user.password)
    access_token = create_access_token(
        data={"sub": str(signed_user.id)},
        expires_delta=timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    )
    refresh_token = create_refresh_token(
        data={"sub": str(signed_user.id)},
        expires_delta=timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES)
    )
    return {
        "access_token": access_token,
        "refresh_token": refresh_token,
        "token_type": "bearer"
    }


async def get_token(dbsession: DBSession, email, password):
    signed_user = await authenticate_user(session=dbsession, email=email, password=password)
    if not signed_user:
        raise CredentialsException
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": str(signed_user.username)}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


async def refresh_all_tokens(
        dbsession: DBSession,
        refresh_token: str
):
    user_session = await get_user_session(dbsession, refresh_token)
    if not user_session:
        raise CredentialsException

    new_refresh_token = create_refresh_token(
        data={"sub": str(user_session.user_id)},
        expires_delta=timedelta(minutes=REFRESH_TOKEN_EXPIRE_MINUTES)
    )
    await update_refresh_token(dbsession, refresh_token, new_refresh_token)
    new_access_token = create_access_token(
        data={"sub": str(user_session.user_id)},
        expires_delta=timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    )
    return {'access_token': new_access_token, 'refresh_token': new_refresh_token}
