from sqlalchemy import select, insert, update
from sqlalchemy.orm import Session as DBSession

from app.models import User, UserSession
from app.user import schema


async def get_user_by_id(dbsession: DBSession, user_id: int) -> User:
    user = await dbsession.get(User, user_id)
    return user


async def get_user_by_email(dbsession: DBSession, email: str) -> User:
    stmt = select(User).where(User.email == email)
    result = await dbsession.execute(stmt)
    user = result.scalar_one_or_none()
    return user


async def create_user(dbsession: DBSession, user: schema.UserCreate) -> User:
    from app.user.auth import get_password_hash
    password_hash = get_password_hash(user.password)
    user_data = {
        "email": user.email,
        "username": user.username,
        "password_hash": password_hash,
    }
    # TODO check possibility to return full user data instead only id.
    stmt = insert(User).values(user_data).returning(User.id)
    result = await dbsession.execute(stmt)
    user_id = result.scalar_one()
    await dbsession.commit()
    user = await get_user_by_id(dbsession=dbsession, user_id=user_id)
    return user


async def create_user_session(dbsession: DBSession, user_session_data: dict) -> None:
    stmt = insert(UserSession).values(user_session_data)
    await dbsession.execute(stmt)
    await dbsession.commit()


async def get_user_session(dbsession: DBSession, refresh_token: str) -> UserSession:
    stmt = select(UserSession).where(UserSession.token == refresh_token)
    result = await dbsession.execute(stmt)
    session = result.scalar_one()
    return session


async def update_refresh_token(dbsession: DBSession, old_token: str, new_token: str) -> None:
    stmt = update(UserSession).filter_by(token=old_token).values(token=new_token)
    await dbsession.execute(stmt)
    await dbsession.commit()
