from fastapi import FastAPI

from app import models
from app.database.database import async_engine
from app.direction import router as direction_router
from app.user import router as user_router

app = FastAPI()


@app.on_event("startup")
async def create_db():
    async with async_engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


app.include_router(direction_router.router)
app.include_router(user_router.router)
