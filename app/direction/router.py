from typing import List, Annotated

from fastapi import APIRouter, Depends, Response
from sqlalchemy.orm import Session as DBSession

from app.direction import crud, schema, utils
from app.database.connection import get_db
from app.user import schema as user_schema
from app.user.auth import get_current_active_user

router = APIRouter(
    prefix="/directions",
    tags=["directions"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[schema.Direction])
async def index(dbsession: DBSession = Depends(get_db)):
    return await crud.get_directions(dbsession=dbsession)


@router.get("/{direction_id}", response_model=schema.Direction)
async def get(direction_id: int, dbsession: DBSession = Depends(get_db)):
    return await crud.get_direction(dbsession=dbsession, direction_id=direction_id)


@router.post("/", response_model=schema.Direction)
async def create(
        direction: schema.DirectionCreate,
        dbsession: DBSession = Depends(get_db),
        current_user: user_schema.User = Depends(get_current_active_user)
):
    direction.owner_id = current_user.id
    return await crud.create_direction(dbsession=dbsession, direction=direction)


@router.put("/{direction_id}", response_class=Response)
async def update(direction_id: int, direction: schema.DirectionCreate, dbsession: DBSession = Depends(get_db)):
    await crud.update_direction(dbsession=dbsession, direction_id=direction_id, direction=direction)


@router.delete("/{direction_id}", status_code=204, response_class=Response)
async def delete(direction_id: int, dbsession: DBSession = Depends(get_db)):
    await crud.delete_direction(dbsession=dbsession, direction_id=direction_id)


# Proxy to Mapbox routing API.
@router.post("/get_from_mapbox_api", response_model=schema.DirectionBase)
async def get_from_here_api(coordinates: List[Annotated[List[float], 2]]):
    direction_data = utils.mapbox_api_request(coordinates=coordinates)
    direction = schema.DirectionBase(**direction_data)
    return direction
