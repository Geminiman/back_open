import json
from typing import Annotated, List, Any, Dict

import requests

from app.exceptions import ExternalAPIException
from app.models import TransportTypeEnum
from app.settings import MAPBOX_DIRECTIONS_API_URL, MAPBOX_API_KEY


def mapbox_api_request(coordinates: List[Annotated[List[float], 2]]) -> Dict[str, Any]:
    coordinates_line = '%3B'.join([f"{(point[1])}%2C{point[0]}" for point in coordinates])
    mapbox_api_url = (
        f'{MAPBOX_DIRECTIONS_API_URL}'
        f'{coordinates_line}'
        f'?alternatives=false&'
        f'geometries=geojson&'
        f'steps=false&'
        f'access_token={MAPBOX_API_KEY}'
    )
    resp = requests.get(mapbox_api_url)
    if resp.status_code != 200:
        raise ExternalAPIException
    response_json = json.loads(resp.content)
    route_points = response_json["routes"][0]["geometry"]["coordinates"]
    length = round(response_json["routes"][0]["distance"]/1000, 1)
    stop_points_titles = [item["name"] for item in response_json["waypoints"]]
    direction_data = {
        "route_points": route_points,
        "stop_points_titles": stop_points_titles,
        "stop_points": coordinates,
        "length": length,
        "transport_type": TransportTypeEnum.car
    }

    return direction_data
