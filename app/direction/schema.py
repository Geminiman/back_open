from datetime import datetime
from typing import List, Tuple, Optional, Annotated

from pydantic import BaseModel

from app.models import TransportTypeEnum


class DirectionBase(BaseModel):
    route_points: List[Annotated[Tuple[float, float], 2]]
    stop_points: List[Annotated[Tuple[float, float], 2]]
    stop_points_titles: List[str]
    length: float
    transport_type: Optional[TransportTypeEnum]

    class Config:
        use_enum_values = True


class DirectionCreate(DirectionBase):
    title: str
    description: str
    is_public: Optional[bool]
    owner_id: Optional[int]


class Direction(DirectionCreate):
    id: int
    created_at: datetime
    updated_at: Optional[datetime]

    class Config:
        orm_mode = True
