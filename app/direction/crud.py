from typing import List

from sqlalchemy import select, update, insert, delete
from sqlalchemy.orm import Session as DBSession

from app.models import Direction
from app.direction import schema


async def get_directions(dbsession: DBSession, skip: int = 0, limit: int = 50) -> List[Direction]:
    stmt = select(Direction).offset(skip).limit(limit)
    result = await dbsession.execute(stmt)
    directions = result.scalars().all()
    return directions


async def get_direction(dbsession: DBSession, direction_id: int) -> Direction:
    direction = await dbsession.get(Direction, direction_id)
    return direction


async def get_user_directions(dbsession: DBSession, user_id: int) -> List[Direction]:
    stmt = select(Direction).where(Direction.owner_id == user_id)
    result = await dbsession.execute(stmt)
    directions = result.scalars().all()
    return directions


async def create_direction(dbsession: DBSession, direction: schema.DirectionCreate) -> Direction:
    stmt = insert(Direction).values(**direction.dict()).returning(Direction.id)
    result = await dbsession.execute(stmt)
    direction_id = result.scalar_one()
    await dbsession.commit()
    direction = await get_direction(dbsession=dbsession, direction_id=direction_id)
    return direction


async def update_direction(dbsession: DBSession, direction_id: int, direction: schema.DirectionCreate) -> None:
    stmt = update(Direction).filter_by(id=direction_id).values(direction.dict())
    await dbsession.execute(stmt)
    await dbsession.commit()


async def delete_direction(dbsession: DBSession, direction_id: int) -> None:
    stmt = delete(Direction).where(Direction.id == direction_id)
    await dbsession.execute(stmt)
    await dbsession.commit()
