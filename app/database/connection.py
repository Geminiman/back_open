from app.database.database import async_session


async def get_db():
    async with async_session() as session:
        try:
            yield session
        finally:
            pass
