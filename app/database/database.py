from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

SQLALCHEMY_DB_URL = "postgresql+asyncpg://xploration_user:xploration_password@localhost/xploration"

async_engine = create_async_engine(SQLALCHEMY_DB_URL, future=True)
async_session = sessionmaker(
        async_engine, expire_on_commit=False, class_=AsyncSession
    )

Base = declarative_base()
